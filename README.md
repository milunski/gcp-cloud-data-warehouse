# FLEX Cloud Data Warehouse Project
This is the code the FLEX Cloud Data Warehouse project with Google Cloud.

_Lingua pura_ is Python.

# Data Flow
Data will stream from Talend -> Cloud Storage -> BigQuery

* Will utilize Cloud Functions and a Finalize / Create as the trigger

# Pycharm Local Dev Setup
  * Requirements
    * GCP Service Account w/ Cloud Storage and BigQuery Permissions
    * Service Accounts needs credentials stored in PyCharm Project Directory
    * Set an Environmental Variable to the file
      * Format: GOOGLE_APPLICATION_CREDENTIALS=cred_file.json

# Open Work Items - Probably needs more detail
- [] CI / CD Pipeline?
- [] Cloud Function Setup
- [] BigQuery Dataset Setup
- [] BigQuery Table Setup
- [] Data Flow Dervicd Tables / Views
- [] Visualizations (Tableau, Data Studio , other?)
- [] Cool AI / ML?
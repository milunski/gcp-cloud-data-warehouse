import os
## Use environment variable to indicate target BigQuery dataset
## This will be different based on dev/prod
ds = os.environ["bq_dataset"]

def load_finance_data(event, context, dataset=ds):
    """
    Background Cloud Function to be triggered by Cloud Storage.
       This generic function logs relevant event data when a file is changed.
       Function will only load the file that was placed into storage.

    Args:
        event (dict): The Cloud Functions event payload.
        context (google.cloud.functions.Context): Metadata of triggering event.
        dataset (string): Default dataset location for the data
    Returns:
        None; the output is written to Stackdriver Logging
    """

    ##### Import Libraries #####
    from google.cloud import bigquery
    from google.cloud import storage

    ##### Capture dataset environ variable, bucket, & file that triggered #####
    trigger_bucket = event['bucket']
    trigger_file = event['name']

    ## Print Cloud Function Trigger details
    print(f"Cloud function triggered by: Bucket - {trigger_bucket}, File - {trigger_file}")

    ## Ignore the creation of a file in loaded
    if str(trigger_file).startswith('loaded'):
        pass
    else:
        ##### Process the data into BQ #####
        ## Setup BiqQuery Client & Dataset
        bq_client = bigquery.Client()
        dataset_ref = bq_client.dataset(dataset)  ## Maybe deprecated?

        ## Setup Job Configuration
        job_config = bigquery.LoadJobConfig()
        job_config.autodetect = False
        job_config.source_format = bigquery.SourceFormat.CSV
        job_config.field_delimiter = ';'
        job_config.skip_leading_rows=1

        ## Loop files, load to proper BQ table, move file to loaded folder
        table = trigger_file.split(sep='/')[0].lower()

        ## Pull beginning table stats
        destination_table = bq_client.get_table(dataset_ref.table(table))
        beginning_rows = destination_table.num_rows

        print(f"Loading File: {trigger_file}")

        ## Load the file
        load_job = bq_client.load_table_from_uri(
            f"gs://{trigger_bucket}/{trigger_file}",
            dataset_ref.table(table),
            job_config=job_config
        )

        ## Print results & rows loaded
        load_job.result()
        destination_table = bq_client.get_table(dataset_ref.table(table))
        ending_rows = destination_table.num_rows

        print(f"Loaded File: {trigger_file}")
        print(f"Rows Loaded: {ending_rows - beginning_rows}")

        ##### Move file into gs://bucket/loaded #####
        ## Setup Storage Client
        cs_client = storage.Client()

        ## Move loaded file in the loaded subfolder
        bucket = cs_client.bucket(trigger_bucket)
        blob = bucket.blob(trigger_file)
        new_blob = bucket.rename_blob(blob, f"loaded/{trigger_file}")
        print("Blob {} has been moved to {}".format(blob.name, new_blob.name))

module "cloud-function" {
    source = "./modules/cloudfunction"
    resource = var.resource
    source_repo_url = var.source_repo_url
    project_id = var.project_id
    function_name = var.function_name
    bq_dataset = var.bq_dataset
}
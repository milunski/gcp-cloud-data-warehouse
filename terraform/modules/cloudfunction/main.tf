provider "google" {
    project = var.project_id
    region  = var.region
    zone    = var.zone
}
resource "google_cloudfunctions_function" "function" {
    name = var.function_name
    labels = {
        creator = "jc-burnworth"
        email = "jc-dot-burnworth-at-slalom-dot-com"
    }  
    project = var.project_id
    description = "Loads Deelo's data to BigQuery. Part of the cloud ETL project."
    runtime = "python37"
    available_memory_mb = 256
    event_trigger {
        event_type = "google.storage.object.finalize"
        resource = var.resource
    }
    source_repository {
        url = var.source_repo_url
    }
    entry_point = "load_finance_data"
    environment_variables = {
        bq_dataset = var.bq_dataset
    }
}
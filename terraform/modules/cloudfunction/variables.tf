variable "resource" {
    description = "Name of resource for trigger. In this case cloud storage bucket."
}

variable "source_repo_url" {
    description = "Url for Source Code Repo that contains function package."
}

variable "project_id" {
    description = "ID of project where topic will be created."
}

variable "region" {
    description = "Region where cloud function is deployed."
    default = "us-central1"
}

variable "zone" {
    description = "Zone for cloud function."
    default = "us-central1-c"
}

variable "function_name" {
    description = "Name of Cloud Function. This will be set through env vars in Cloud Build to properly separate dev and prod builds."
}

variable "bq_dataset" {
    description = "Target dataset for File Loads. This will be set by an environment variable."
}
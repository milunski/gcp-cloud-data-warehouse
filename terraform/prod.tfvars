# Cloudfunction vars
resource="projects/gcp-stl/buckets/mdeelo_cloudengineering"
project_id="gcp-stl"
bq_dataset="gcp_flex_etl"
source_repo_url="https://source.developers.google.com/projects/gcp-stl/repos/bitbucket_milunski_gcp-cloud-data-warehouse/moveable-aliases/master/paths/cloudfunctions/src"
function_name="cloudfunction-cloud-etl-prod"
